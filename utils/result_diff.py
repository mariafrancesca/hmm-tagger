from pprint import PrettyPrinter


def get_diff(test, gold):
    differences = []
    i = j = 0

    while i < len(test) and j < len(gold):
        t = test[i]
        g = gold[j]

        if t[0] != g[0]:
            j = j + 1
            continue

        if g[1] == '-':
            i = i + 1
            j = j + 1
            continue

        if t[1] != g[1]:
            differences.append('{} -> {}'.format(g[1], t[1]))

        i = i + 1
        j = j + 1

    # if i < len(test) or j < len(gold):
    #     print('Something went wrong with this input:')
    #     pp.pprint(test)
    #     pp.pprint(gold)

    return differences
