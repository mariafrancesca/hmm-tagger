# -*- coding: utf-8 -*-

# CoNLL-U column names
ID, FORM, LEMMA, UPOS, XPOS, FEATS, HEAD, DEPREL, DEPS, MISC = range(10)


# UD Error is used when raising exceptions in this module
class UDError(Exception):
    pass


def load_conllu(file):
    last_sentence_text = ''
    last_sentence = []
    sentences = []

    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")

        # Read the sentence text
        if line.startswith("# text = "):
            last_sentence_text = line[9:]

        if line.startswith("#"):
            continue

        if not line:
            sentences.append({"text": last_sentence_text, "tokens": last_sentence})
            # sentences.append({"text": last_sentence_text.upper(), "tokens": last_sentence})
            last_sentence = []
            continue

        # Read next token/word
        columns = line.split("\t")
        if len(columns) != 10:
            raise UDError("The CoNLL-U line does not contain 10 tab-separated columns: '{}'".format(line))

        # Skip empty nodes
        if "." in columns[ID]:
            continue

        # Delete spaces from FORM  so gold.characters == system.characters
        # even if one of them tokenizes the space.
        columns[FORM] = columns[FORM].replace(" ", "")
        if not columns[FORM]:
            raise UDError("There is an empty FORM in the CoNLL-U file")

        last_sentence.append((columns[FORM], columns[UPOS]))
        # last_sentence.append((columns[FORM].upper(), columns[UPOS].upper()))

    return sentences


def load_conllu_to_tagged_sentences(file):
    result = load_conllu(file)
    return [item['tokens'] for item in result]
