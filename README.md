# HMM POS Tagger

POS Tagger per l'esame di Mazzei

## Istruzioni

Posizionarsi nella cartella del progetto ed eseguire i seguenti comendi in una shell:

```
$ mkdir venv
$ python3 -m venv ./venv/hmm
$ source ./venv/hmm/bin/activate
```

Fatto questo bisogna installare le dipendenze:

```
$ pip install -r requirements.txt
```

Se è la prima volta che viene installato NLTK, assicurarsi di scaricare i treebank di esempio.
Eseguire il comando `python` ed inserire questi comandi:

```
>>> import nltk
>>> nltk.download('treebank')
>>> nltk.download('punkt')
```