# -*- coding: utf-8 -*-

from collections import Counter
from pprint import PrettyPrinter

import numpy as np
from nltk import unique_list, MLEProbDist, FreqDist, ConditionalFreqDist, ConditionalProbDist, word_tokenize

from utils.conllu_reader import load_conllu_to_tagged_sentences, load_conllu
from utils.result_diff import get_diff

_TEXT = 0  # index of text in a tuple
_TAG = 1  # index of tag in a tuple


class POSTagger(object):
    """
    Hidden Markov POS tagger, a generative model for labelling sequence data.
    These models define the joint probability of a sequence of symbols and
    their labels (state transitions) as the product of the starting state
    probability, the probability of each state transition, and the probability
    of each observation being generated from each state.

    This implementation is based on the HMM description in Chapter 8, Huang,
    Acero and Hon, Spoken Language Processing.

    :param symbols: the set of output symbols (alphabet)
    :type symbols: seq of any
    :param states: a set of states representing state space
    :type states: seq of any
    :param transitions: transition probabilities; Pr(s_i | s_j) is the
        probability of transition from state i given the model is in
        state_j
    :type transitions: ConditionalProbDistI
    :param outputs: output probabilities; Pr(o_k | s_i) is the probability
        of emitting symbol k when entering state i
    :type outputs: ConditionalProbDistI
    :param priors: initial state distribution; Pr(s_i) is the probability
        of starting in state i
    :type priors: ProbDistI
    """

    def __init__(self, symbols, states, transitions, outputs, priors):
        self._symbols = unique_list(symbols)
        self._states = unique_list(states)
        self._transitions = transitions
        self._outputs = outputs
        self._priors = priors
        self._cache = None

    def tag(self, unlabeled_sequence):
        path = self._best_path_simple(unlabeled_sequence)
        return list(zip(unlabeled_sequence, path))

    def _output_logprob(self, state, symbol):
        """
        :return: the log probability of the symbol being observed in the given
            state
        :rtype: float
        """
        return self._outputs[state].logprob(symbol)

    def _best_path_simple(self, unlabeled_sequence):
        """
        Simple implementation of the Viterbi algorithm
        :param unlabeled_sequence:  starting input
        :return: labelled sequence Word-Tag
        """
        T = len(unlabeled_sequence)
        N = len(self._states)
        V = np.zeros((T, N), np.float64)
        B = {}

        # find the starting log probabilities for each state
        symbol = unlabeled_sequence[0]
        for i, state in enumerate(self._states):
            V[0, i] = self._priors.logprob(state) + \
                      self._output_logprob(state, symbol)
            B[0, state] = None

        # find the maximum log probabilities for reaching each state at time t
        for t in range(1, T):
            symbol = unlabeled_sequence[t]
            for j in range(N):
                sj = self._states[j]
                best = None
                for i in range(N):
                    si = self._states[i]
                    va = V[t - 1, i] + self._transitions[si].logprob(sj)
                    if not best or va > best[0]:
                        best = (va, si)
                V[t, j] = best[0] + self._output_logprob(sj, symbol)
                B[t, sj] = best[1]

        # find the highest probability final state
        best = None
        for i in range(N):
            val = V[T - 1, i]
            if not best or val > best[0]:
                best = (val, self._states[i])

        # traverse the back-pointers B to find the state sequence
        current = best[1]
        sequence = [current]
        for t in range(T - 1, 0, -1):
            last = B[t, current]
            sequence.append(last)
            current = last

        sequence.reverse()
        return sequence

    def __repr__(self):
        return ('<POSTagger %d states and %d output symbols>'
                % (len(self._states), len(self._symbols)))


class HiddenMarkovModelTrainer(object):
    """
    Creates an HMM trainer to induce an HMM with the given states and
    output symbol alphabet
    :param states:  the set of state labels
    :type states:   sequence of any
    :param symbols: the set of observation symbols
    :type symbols:  sequence of any
    """

    def __init__(self, states=None, symbols=None):
        self._states = (states if states else [])
        self._symbols = (symbols if symbols else [])

    def train(self, labelled_sequences, estimator=None):
        """
        Supervised training maximising the joint probability of the symbol and
        state sequences. This is done via collecting frequencies of
        transitions between states-symbol observations while within each
        state and which states start a sentence. These frequency distributions
        are then normalised into probability estimates, which can be
        smoothed if desired.
        :return: the trained model
        :rtype: POSTagger
        :param labelled_sequences: the training data, a set of
            labelled sequences of observations
        :type labelled_sequences: list
        :param estimator: a function taking
            a FreqDist and a number of bins and returning a CProbDistI;
            otherwise a MLE estimate is used
        """

        # default to the MLE estimate
        if estimator is None:
            estimator = lambda fdist, bins: MLEProbDist(fdist)

        # count occurrences of starting states, transitions out of each state
        # and output symbols observed in each state
        known_symbols = set(self._symbols)
        known_states = set(self._states)

        starting = FreqDist()
        transitions = ConditionalFreqDist()
        outputs = ConditionalFreqDist()

        for sequence in labelled_sequences:
            lasts = None
            for token in sequence:
                state = token[_TAG]
                symbol = token[_TEXT]
                if lasts is None:
                    starting[state] += 1
                else:
                    transitions[lasts][state] += 1
                outputs[state][symbol] += 1
                lasts = state

                # update the state and symbol lists
                if state not in known_states:
                    self._states.append(state)
                    known_states.add(state)

                if symbol not in known_symbols:
                    self._symbols.append(symbol)
                    known_symbols.add(symbol)

        # create probability distributions (with Laplace smoothing)
        N = len(self._states)  # number of the states
        pi = estimator(starting, N)  # our estimator
        A = ConditionalProbDist(transitions, estimator, N)  # pdf of transitions
        B = ConditionalProbDist(outputs, estimator, len(self._symbols))  # smoothed pdf

        return POSTagger(self._symbols, self._states, A, B, pi)


if __name__ == '__main__':
    train_data = []
    test_data = []
    errors = []
    pp = PrettyPrinter(indent=2)

    # Load train data
    with open('it_isdt-ud-train.conllu', mode='r', encoding='UTF-8') as conllu:
        train_data = load_conllu_to_tagged_sentences(conllu)

    # Load test data
    with open('it_isdt-ud-test.conllu', mode='r', encoding='UTF-8') as conllu:
        test_data = load_conllu(conllu)

    trainer = HiddenMarkovModelTrainer()
    tagger = trainer.train(train_data)
    total_tokens = 0

    for sentence in test_data:
        text = sentence['text']
        gold = sentence['tokens']
        test = tagger.tag(word_tokenize(text, language='italian'))
        errors.extend(get_diff(test, gold))
        total_tokens += len(gold)

    accuracy = (1 - len(errors) / total_tokens) * 100
    print('Accuracy is:\t{0:.2f}%'.format(accuracy))
    print('Baseline is:\t90%')
    print('The most common errors are:')
    pp.pprint(Counter(errors))

